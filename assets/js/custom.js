$(document).ready(function(){
  $('.bxslider').bxSlider({
  	 mode: 'fade',
  	 captions: true
  });
});

$(document).ready(function(){
  $('.slider1').bxSlider({
    slideWidth: 400,
    slideHeight: 100,
    minSlides: 2,
    maxSlides: 3,
    slideMargin: 10,
    pager: false
  });
});